package model;

import java.util.*;

public interface CountMap<T> extends Iterable<T> {
    void add(T o);

    int getCount(T o);

    int remove(T o);

    int size();

    void addAll(CountMap<T> source);

    Map toMap();

    void toMap(Map<? super T, Integer> destination);
}