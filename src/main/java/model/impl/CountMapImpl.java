package model.impl;

import model.CountMap;

import java.util.*;

public class CountMapImpl<T> implements CountMap<T> {

    private final List<T> list = new ArrayList<>();
    Map<T, Integer> map = new HashMap<>();

    @Override
    public void add(T o) {
        list.add(o);
    }

    @Override
    public int getCount(T o) {
        int count = 0;
        for (T item : list) {
            if (item == o) count++;
        }
        return count;
    }

    @Override
    public int remove(T o) {
        list.removeIf(list -> list.equals(o));
        return getCount((T) list);
    }

    @Override
    public int size() {
        return (int) list.stream().distinct().count();
    }

    @Override
    public void addAll(CountMap<T> source) {
        for (T t : source) {
            list.add(t);
        }
    }

    @Override
    public Map<T, Integer> toMap() {
        HashMap<T, Integer> objectObjectHashMap = new HashMap<>();
        list.forEach(t -> objectObjectHashMap.put(t, getCount(t)));
        return objectObjectHashMap;
    }

    @Override
    public void toMap(Map<? super T, Integer> destination) {
        for (Map.Entry<T, Integer> entry : map.entrySet()) {
            T key = entry.getKey();
            destination.put(key, destination.getOrDefault(key, 0) + map.get(key));
        }
    }


    @Override
    public Iterator<T> iterator() {
        return list.iterator();
    }
}